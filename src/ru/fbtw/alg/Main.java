package ru.fbtw.alg;

import java.io.PrintStream;
import java.util.Scanner;

public class Main {

	static PrintStream out = System.out;

	private static final char sym1 = '*';
	private static final char sym2 = '#';
	private static final char sep = ' ';

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		out.print("Введите n: ");
		int n = in.nextInt();

		printSendWatch(n);
	}


	/**
	 *
	 *        ******    - Верхняя линия
	 *        *###*
	 *        *##*
	 *        *#*       - Верхняя часть до серидины
	 *        **
	 *        *
	 *       *
	 *      **
	 *     *#*          - Нижняя часть до середины
	 *    *##*
	 *   *###*
	 *  ****** 			- Нижняя линяя
	 *
	 * */

	public static void printSendWatch(int n) {
		int tmp = n / 2;

		// Рисуем верхнюю линюю
		printSymbols(tmp, sep);
		printSymbols(tmp, sym1);
		out.println();

		// Верхняя часть до серидины
		for (int i = 1; i < tmp; i++) {
			printSymbols(tmp, sep);
			printMainString(tmp - i);
			out.println();
		}

		// Нижняя часть до середины
		for (int i = 1; i < tmp; i++) {
			printSymbols(tmp - i, sep);
			printMainString(i);
			out.println();
		}

		// Нижняя линяя
		printSymbols(tmp, sym1);
		printSymbols(tmp, sep);
	}

	/**
	 * Рисует a символов ch
	 *
	 * */
	private static void printSymbols(int a, char ch) {
		for (int i = 0; i < a; i++) {
			out.print(ch);
		}
	}

	/**
	 * Рисует строку вида *#######*
	 *
	 * Для a = 1 рисует *
	 *
	 * */
	private static void printMainString(int a) {
		out.print(sym1);

		if (a > 1) {
			printSymbols(a - 2, sym2);
			out.print(sym1);
		}
	}


}
